﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player1PaddleScript : MonoBehaviour {

    void Start()
    {
        //get a reference to the ball in the scene
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 mousePosition = Input.mousePosition;

        Vector3 paddlePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        //screen edges
        float screenEdge = Camera.main.orthographicSize * Camera.main.aspect;

        //only keep the x position of the mouse, and keep the Y position as -4
        transform.position = new Vector3(-7f, Mathf.Clamp(paddlePosition.y, -3f, 3f));
    }
}
