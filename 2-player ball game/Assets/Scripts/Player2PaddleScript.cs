﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player2PaddleScript : MonoBehaviour {

    public float moveSpeed = 8f;

    public float topBounds = 3f;

    public float bottomBounds = -3f;

    public Vector2 startingPosition = new Vector2(-7f, 0f);

    // Use this for initialization
    void Start()
    {
        transform.localPosition = (Vector3)startingPosition;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckUserInput();
    }

    void CheckUserInput()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (transform.localPosition.y > topBounds)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, topBounds, transform.localPosition.z);
            }
            else
            {
                transform.localPosition += Vector3.up * moveSpeed * Time.deltaTime;
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            if (transform.localPosition.y < bottomBounds)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, bottomBounds, transform.localPosition.z);
            }
            else
            {
                transform.localPosition += Vector3.down * moveSpeed * Time.deltaTime;
            }
        }
    }
}
