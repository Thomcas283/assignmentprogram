﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ballController2 : MonoBehaviour
{

    Text scoreText1;
    Text scoreText2;

    public int score1 = 0;
    public int score2 = 0;

    private Rigidbody2D ball;

    // Use this for initialization
    void Start()
    {
        ball = GetComponent<Rigidbody2D>();
        ball.velocity = new Vector2(10f, 10f);
        scoreText1 = GameObject.Find("Player1Score").GetComponent<Text>();
        scoreText2 = GameObject.Find("Player2Score").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText1.text = "Score :" + score1;
        scoreText2.text = "Score :" + score2;

        if (score1 == 5)
        {
            SceneManager.LoadScene("Level 3");
        }
        else if (score2 == 5)
        {
            SceneManager.LoadScene("Level 3");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Net1")
        {
            ball.transform.position = new Vector3(0f, 0f);
            score2 += 1;
            ballController.count2+=1;
        }
        else if (collision.gameObject.tag == "Net2")
        {
            ball.transform.position = new Vector3(0f, 0f);
            score1 += 1;
            ballController.count1+=1;
        }
    }
}

