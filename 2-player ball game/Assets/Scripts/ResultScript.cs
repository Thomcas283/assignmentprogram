﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScript : MonoBehaviour {

    Text winnerText;

    // Use this for initialization
    void Start () {
        winnerText = GameObject.Find("Final Result").GetComponent<Text>();

    }
	
	// Update is called once per frame
	void Update () {
        if (ballController.count1 > ballController.count2)
        {
            winnerText.text = "Player 1 Wins with " + (ballController.count1);
        }
        else if (ballController.count2 > ballController.count1)
        {
            winnerText.text = "Player 2 Wins with " + (ballController.count2);
        }
        else if (ballController.count1 == ballController.count2)
        {
            winnerText.text = "The Game is a Draw!";
        }
    }
}
