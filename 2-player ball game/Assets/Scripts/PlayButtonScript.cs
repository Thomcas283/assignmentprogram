﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayButtonScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Button>().onClick.AddListener(() => loadGame());
    }
	
    void loadGame()
    {
        SceneManager.LoadScene("Level 1");
    }

	// Update is called once per frame
	void Update () {
		
	}
}
